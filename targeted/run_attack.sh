#!/bin/bash
#
# run_attack.sh is a script which executes the attack
#
# Envoronment which runs attacks and defences calls it in a following way:
#   run_attack.sh INPUT_DIR OUTPUT_DIR MAX_EPSILON
# where:
#   INPUT_DIR - directory with input PNG images
#   OUTPUT_DIR - directory where adversarial images should be written
#   MAX_EPSILON - maximum allowed L_{\infty} norm of adversarial perturbation
#

INPUT_DIR=$1
OUTPUT_DIR=$2
MAX_EPSILON=$3

if [ ${MAX_EPSILON} -lt 6 ]
then
  python python/run_cw_inspired.py \
  --input_dir="${INPUT_DIR}" \
  --output_dir="${OUTPUT_DIR}" \
  --max_epsilon="${MAX_EPSILON}" \
  --targeted --n_iter 50 --lr 0.2 --no_augmentation --time_limit_per_100 475 --ensemble adv_inception_resnet_v2 inception_v3_tf --ensemble_weights 4.0 1.0 --checkpoint_paths adv_inception_resnet_v2.pth inception_v3_rw.pth
elif [ ${MAX_EPSILON} -lt 10 ]
then
    python python/run_cw_inspired.py \
  --input_dir="${INPUT_DIR}" \
  --output_dir="${OUTPUT_DIR}" \
  --max_epsilon="${MAX_EPSILON}" \
  --targeted --n_iter 50 --lr 0.2 --no_augmentation --time_limit_per_100 475 --ensemble adv_inception_resnet_v2 inception_v3_tf adv_inception_v3 --ensemble_weights 1.0 2.0 1.0 --checkpoint_paths adv_inception_resnet_v2.pth inception_v3_rw.pth adv_inception_v3_rw.pth
elif [ ${MAX_EPSILON} -lt 14 ]
then
    python python/run_cw_inspired.py \
  --input_dir="${INPUT_DIR}" \
  --output_dir="${OUTPUT_DIR}" \
  --max_epsilon="${MAX_EPSILON}" \
  --targeted --n_iter 50 --lr 0.2 --no_augmentation --time_limit_per_100 475 --ensemble adv_inception_resnet_v2 inception_v3_tf adv_inception_v3 --ensemble_weights 4.0 1.0 1.0 --checkpoint_paths adv_inception_resnet_v2.pth inception_v3_rw.pth adv_inception_v3_rw.pth
else
    python python/run_cw_inspired.py \
  --input_dir="${INPUT_DIR}" \
  --output_dir="${OUTPUT_DIR}" \
  --max_epsilon="${MAX_EPSILON}" \
  --targeted --n_iter 50 --lr 0.2 --no_augmentation --time_limit_per_100 475 --ensemble adv_inception_resnet_v2 inception_v3_tf adv_inception_v3 --ensemble_weights 4.0 1.0 1.0 --checkpoint_paths adv_inception_resnet_v2.pth inception_v3_rw.pth adv_inception_v3_rw.pth
fi
