""" A few fb.resnet.torch like tranforms and more
fb.resenet style transforms taken from https://github.com/pytorch/vision/pull/27
"""
import torch
import random
from PIL import Image
import math
import numpy as np


class Grayscale(object):

    def __call__(self, img):
        gs = img.clone()
        gs[0].mul_(0.299).add_(0.587, gs[1]).add_(0.114, gs[2])
        gs[1].copy_(gs[0])
        gs[2].copy_(gs[0])
        return gs


class Saturation(object):

    def __init__(self, var):
        self.var = var

    def __call__(self, img):
        gs = Grayscale()(img)
        alpha = random.uniform(0, self.var)
        return img.lerp(gs, alpha)


class Brightness(object):

    def __init__(self, var):
        self.var = var

    def __call__(self, img):
        gs = img.new().resize_as_(img).zero_()
        alpha = random.uniform(0, self.var)
        return img.lerp(gs, alpha)


class Contrast(object):

    def __init__(self, var):
        self.var = var

    def __call__(self, img):
        gs = Grayscale()(img)
        gs.fill_(gs.mean())
        alpha = random.uniform(0, self.var)
        return img.lerp(gs, alpha)


class RandomOrder(object):
    """ Composes several transforms together in random order.
    """

    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, img):
        if self.transforms is None:
            return img
        order = torch.randperm(len(self.transforms))
        for i in order:
            img = self.transforms[i](img)
        return img


class ColorJitter(RandomOrder):

    def __init__(self, brightness=0.4, contrast=0.4, saturation=0.4):
        self.transforms = []
        if brightness != 0:
            self.transforms.append(Brightness(brightness))
        if contrast != 0:
            self.transforms.append(Contrast(contrast))
        if saturation != 0:
            self.transforms.append(Saturation(saturation))



class BboxCrop:
    def __init__(self, size, interpolation=Image.BILINEAR, preserve_aspect=False):
        self.size = size
        self.interpolation = interpolation
        self.preserve_aspect = preserve_aspect

    def __call__(self, img, bbox=(0., 0., 1., 1.)):
        w, h = img.size
        th, tw = self.size
        bb_xmin = int(math.floor(bbox[0] * w))
        bb_ymin = int(math.floor(bbox[1] * h))
        bb_xmax = int(math.ceil(bbox[2] * w))
        bb_ymax = int(math.ceil(bbox[3] * h))
        bb_w = bb_xmax - bb_xmin
        bb_h = bb_ymax - bb_ymin
        bb_cx = bb_xmin + bb_w // 2
        bb_cy = bb_ymin + bb_h // 2

        if self.preserve_aspect:
            tr = tw / th
            br = bb_w / bb_h

            rr = tr / br

            if bb_w >= bb_h:
                bb_h = int(round(bb_w * th / tw))
            else:
                bb_w = int(round(bb_w * tw / th))

            if tw >= th:
                if bb_w >= bb_h:
                    bb_h = int(round(bb_w * th / tw))
                else:
                    bb_w = int(round(bb_w * tw / th))
            else:
                if bb_h >= bb_w:
                    bb_w = int(round(bb_h * th / tw))
                else:
                    bb_h = int(round(bb_h * tw / th))
            bb_xmin = bb_cx - bb_w // 2
            bb_ymin = bb_cy - bb_h // 2
            if bb_xmin < 0:
                bb_xmin = 0
            if bb_ymin < 0:
                bb_ymin = 0
            bb_xmax = bb_xmin + bb_w
            bb_ymax = bb_ymin + bb_h
            cropped = img.crop((bb_xmin, bb_ymin, bb_xmax, bb_ymax))
        else:
            cropped = img.crop((bb_xmin, bb_ymin, bb_xmax, bb_ymax))

        return cropped.resize((tw, th), self.interpolation)


        x1 = int(round((w - tw) / 2.))
        y1 = int(round((h - th) / 2.))




class RandomBboxCrop:

    def __init__(
            self, size, interpolation=Image.BILINEAR,
            aspect_range=(1.0, 1.0),
            area_range=(0.5, 1.0),
            min_coverage=0.8):

        self.size = size
        self.interpolation = interpolation
        self.aspect_range = aspect_range
        self.area_range = area_range
        self.min_coverage = 0.8

    def __call__(self, img, bbox=(0., 0., 1., 1.)):
        return img


